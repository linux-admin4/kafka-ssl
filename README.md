# Документация по развертыванию кластера Kafka с авторизацией по SSL

## Введение
Эта документация описывает шаги по развертыванию кластера Apache Kafka с настройкой SSL авторизации, включая создание SSL-сертификатов, конфигурацию брокеров и клиентов.

## 1. Настройка брокеров Kafka
Сначала необходимо установить и настроить брокеры Kafka. Для этого выполните следующие шаги:

1. Скачайте Kafka с официального сайта Apache Kafka.
2. Распакуйте архив с Kafka.
3. Настройте файл `server.properties` для каждого брокера.

## 2. Создание SSL-сертификатов
Создайте SSL-сертификаты для каждого брокера и клиентов. Вы можете использовать инструменты OpenSSL или keytool из Java. В этом примере будем использовать OpenSSL:

```bash
# Создание приватного ключа для CA (удостоверяющего центра)
openssl genrsa -out ca-key.pem 2048

# Создание сертификата CA
openssl req -new -x509 -key ca-key.pem -out ca-cert.pem -days 365

# Создание приватного ключа для брокера
openssl genrsa -out broker-key.pem 2048

# Создание запроса на сертификат (CSR) для брокера
openssl req -new -key broker-key.pem -out broker-csr.pem

# Подписание CSR брокера сертификатом CA
openssl x509 -req -in broker-csr.pem -CA ca-cert.pem -CAkey ca-key.pem -CAcreateserial -out broker-cert.pem -days 365
```

## 3. Конфигурация брокеров Kafka
Настройте каждый брокер Kafka для использования SSL-сертификатов. Измените файл `server.properties` для каждого брокера:

```properties
# Настройки SSL
ssl.keystore.location=/path/to/keystore.jks
ssl.keystore.password=your_keystore_password
ssl.key.password=your_key_password
ssl.truststore.location=/path/to/truststore.jks
ssl.truststore.password=your_truststore_password
ssl.client.auth=required

# Включение SSL listeners
listeners=SSL://broker1:9093
advertised.listeners=SSL://broker1:9093
security.inter.broker.protocol=SSL
```

Создайте файлы keystore и truststore с помощью keytool:

```bash
# Создание keystore
keytool -keystore kafka.server.keystore.jks -alias localhost -validity 365 -genkey

# Импорт сертификата CA в truststore
keytool -keystore kafka.server.truststore.jks -alias CARoot -import -file ca-cert.pem

# Импорт сертификата брокера в keystore
keytool -keystore kafka.server.keystore.jks -alias localhost -import -file broker-cert.pem

# Импорт сертификата CA в keystore
keytool -keystore kafka.server.keystore.jks -alias CARoot -import -file ca-cert.pem
```

## 4. Конфигурация клиентов Kafka
Настройте ваших клиентов Kafka для использования SSL. Пример для Kafka producer:

```java
Properties props = new Properties();
props.put("bootstrap.servers", "broker1:9093");
props.put("security.protocol", "SSL");
props.put("ssl.keystore.location", "/path/to/keystore.jks");
props.put("ssl.keystore.password", "your_keystore_password");
props.put("ssl.key.password", "your_key_password");
props.put("ssl.truststore.location", "/path/to/truststore.jks");
props.put("ssl.truststore.password", "your_truststore_password");

KafkaProducer<String, String> producer = new KafkaProducer<>(props);
```

## 5. Запуск брокеров Kafka
Запустите каждый брокер Kafka с настроенными параметрами SSL:

```bash
bin/kafka-server-start.sh config/server.properties
```

## 6. Проверка настройки
Произведите и потребляйте сообщения с использованием настроек SSL, чтобы убедиться в правильности конфигурации.

## Резюме
1. Настройка брокеров Kafka.
2. Генерация SSL-сертификатов с использованием OpenSSL или keytool.
3. Конфигурация каждого брокера с использованием сгенерированных сертификатов.
4. Настройка клиентов Kafka для использования SSL.
5. Запуск брокеров и проверка конфигурации.

Эти шаги являются обобщенным руководством. В каждом конкретном случае могут потребоваться дополнительные настройки в зависимости от среды и требований. Для производственных сред рекомендуется автоматизировать эти шаги и интегрировать их с системой управления сертификатами.